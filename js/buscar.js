$(document).ready(function(){

	let resultados = $("#resultados");
	resultados.hide(0);
	let generos;

	$.ajax({   
		url: "https://api.themoviedb.org/3/genre/movie/list?api_key=3356865d41894a2fa9bfa84b2b5f59bb&language=es",
		success: function(respuesta) {
			console.log(respuesta);
			generos = respuesta
		},
		error: function() {
			console.log("No se ha podido obtener la información");
		},
	});

	$("#btn-buscar").on("click", function(){
		// Obtenemos la palabra
		const palabra = $('#busqueda').val();

		// Es importante validar, validamos que la palabra no este vacía
		if(palabra!==""){
			console.log('Palabra a buscar: '+palabra);
			alert('Vamos a buscar: '+palabra);
			
			// Mostramos el contenedor para la lista
			resultados.show(1000);
			// Llamamos a nuestra función para realizar la llamada
			enviar_solicitud_lista(palabra,generos);
		// En caso de que el campo se haya dejado vacío
		}else{
			console.log('ERROR: No se especificó el campo "busqueda"');
			alert('Introduce una palabra');
		}
		

	});

});

// Función para realizar la llamada
function enviar_solicitud_lista(palabra, generos){
	$.ajax({
		// url para petición, cambié el APIKey por el que viene en el ejemplo
		url: "http://api.themoviedb.org/3/search/movie?&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
		success: function(respuesta) {
			console.log(respuesta);
			peliculas = $('#content-lista'); //DIV donde se cargará la lista de peliculas

			setTimeout(function () {
				$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)
				let generos_pelicula;
				//Para cada elemento en la lista de resultados (para cada pelicula)
				$.each(respuesta.results, function(index, elemento) {
					// Aquí determinamos los generos de las peliculas
					generos_pelicula = "";
					$.each(generos.genres, function(indice, gen) {
						if(elemento.genre_ids.includes(gen.id)){
							generos_pelicula += gen.name+" ";
						}
					});
					// Creamos nuestro texto para HTML y lo agregamos
					cardHTML = crearMovieCard(elemento, generos_pelicula); 
					peliculas.append(cardHTML);
				});

			}, 3000); //Tarda 3 segundos en ejecutar la función de callback
			          //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.

		},
		error: function() {
			console.log("No se ha podido obtener la información");
			$('#loader').remove();
			$('#content-lista').html('No se ha podido obtener la información');
		},
		beforeSend: function() { 
			//ANTES de hacer la petición se muestra la imagen de cargando.
			console.log('CARGANDO');
			$('#content-lista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
		},
	});
}

function crearMovieCard(movie, generos){
	//Llega el objeto JSON de UNA película, como la regresa la API
    //sabemos que el directorio donde se guardan las imágenes es: https://image.tmdb.org/t/p/w500/
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

    //NOTAR que se accede al objeto JSON movie con la notación de punto para acceder a los atributos (movie.original_title).
	// Se agregan otros elementos para poder tener una descripción más completa
	var cardHTML =
		'<!-- CARD -->'
		+'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                +'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>'+movie.vote_average+'</p>'
		                +'</div>'
		                +'<div class="col-8 metadata">'+generos+'</div>'
		             +'</div>'
		          +'</div>'
		          +'<p class="card-text">'+movie.overview+'</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';

		return cardHTML;
}
