# Practica Final con Javascript
Aquí se encuentra el código modificado para la solución de la práctica final del curso de Javascript.

## Descripción-Solución
* Influencié en gran manera mi solución en el código presentado por el instructor para la ventana index.html.
* En vez de realizar una simple lista, generé también un catalogo de resultados. Aquí modifiqué el HTML proporcionado para 
poder tener un contenedor en condiciones para agregar las tarjetas bootstrap. 
* Para la llamada AJAX, utilicé el URL 
presentada en las instrucciones de la práctica, cambiando la APIKey para que funcionara. Coloqué la llamada a AJAX en una
función para que no quedara un código muy extenso, ya que agregué algunas sencillas validaciones para el ejercicio.
* Al ver los objetos en la consola del navegador, la respuesta de la petición con AJAX, me llamó la atención los demás
atributos con los que contaba el objeto de respuestas. Es por ello que modifiqué la función `crearMovieCard` para 
ser capaz de mostrar estos elementos. 
* Me auxilié de otra especificación de la API para determinar el nombre de los generos
que devuelve para cada pelicula, ya que solo se otorga el id de estos. Más información [aquí](https://www.themoviedb.org/talk/5daf6eb0ae36680011d7e6ee?language=es-MX).

## Autor
* Lazaro Martinez Abraham Josue 

### Contacto
abrahamlazaro@comunidad.unam.mx
